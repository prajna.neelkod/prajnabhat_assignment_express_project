const userController = require("../controllers/userController.js");

const router = require("express").Router();

router.get("/search/user/:name/:limit/:sortorder", userController.searchUserByname);
router.get("/get/user", userController.getUser);
router.post("/add/user", userController.addUser);

module.exports = router;

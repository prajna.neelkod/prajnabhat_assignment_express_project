
const userModel = require("../models/user.js");

const searchUserByname = async (req, res) => {
    console.log("Search User");

    var name = req.params.name;
    var page = req.params.limit;
    var sortingOrder = (req.params.sortorder).toUpperCase();
    var sort = sortingOrder == "ASCENDING" ? 1 : -1;

    //every time the number of response data returned will be 20
    var dataCount = 20;
    //based on the page number setting the value to be skipped
    var skip = page * 20;
    try {
        //get all the users whose name is matching with req parameter name
        //sorting the results based on age in descending order
        //limit the response based on the page number
        const user = await userModel.find({ name: new RegExp(name, 'i') }).sort({ 'age': sort }).skip(skip).limit(dataCount);
        return res.send(user);
    } catch (e) {
        return res.send(e);
    }
}

const getUser = async (req, res) => {
    //get list of all the users
    const users = await userModel.find({});
    return res.send(users);
}
const addUser = async (req, res) => {
    console.log(req.body);
    const { name, age } = req.body;
    const newuser = await userModel.create({ name, age });
    return res.send(newuser);
}

module.exports = { getUser, addUser, searchUserByname }
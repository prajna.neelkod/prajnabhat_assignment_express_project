const express = require("express");
const cors = require("cors");
const mongoose = require('mongoose');
const Router = require("./routes/router.js");

require("dotenv").config();
const PORT = process.env.PORT || 3003;
const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


const mongoString = process.env.DATABASE_URL;
mongoose.set("strictQuery", false);
//connection with mongodb data base
mongoose.connect(mongoString);
// const database = mongoose.connection;

app.use(cors());
app.use(Router);
//server will be running on port specified
app.listen(PORT, () => console.log(`Server Started Running on Port ${PORT}`));